#pragma once

#include <vector>
#include <GL/glew.h>

struct Texture
{
	Texture(uint32_t width, uint32_t height, std::vector<unsigned char> image) :
		width(width), height(height), image(image)
	{}

  Texture(const char* filePath);

	uint32_t width, height;
	std::vector<unsigned char> image;

  GLuint loadGlTexture();
};

const Texture whiteTexture = Texture(1, 1, {255, 255, 255, 255});
const Texture normalNormalMap = Texture(1, 1, {128, 128, 255, 255});
