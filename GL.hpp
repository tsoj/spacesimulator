#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <string>
#include <initializer_list>
#include <map>

namespace GL
{
  class Texture2D
  {
    friend class Framebuffer;
  public:
    Texture2D(GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type);
    Texture2D(std::string path);
    ~Texture2D();
    void setParameter(GLenum pname, GLint param);
    void bind(GLenum textureUnit) const;
  private:
    GLuint ID;
  };
  class Texture2DArray
  {
    friend class Framebuffer;
  public:
    Texture2DArray(GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth);
    Texture2DArray() : ID(0) {}
    ~Texture2DArray();
    void setParameter(GLenum pname, GLint param);
    void bind(GLenum textureUnit) const;
  private:
    GLuint ID;
  };
  class Renderbuffer
  {
    friend class Framebuffer;
  public:
    Renderbuffer(GLenum internalformat, GLsizei width, GLsizei height)
    {
      glGenRenderbuffers(1, &ID);
      glBindRenderbuffer(GL_RENDERBUFFER, ID);
      glRenderbufferStorage(GL_RENDERBUFFER, internalformat, width, height);
    }
    ~Renderbuffer()
    {
      glDeleteRenderbuffers(1, &ID);
    }
  private:
    GLuint ID;
  };
  class Framebuffer
  {
  public:
    Framebuffer();
    ~Framebuffer();
    void attach(const Texture2D& texture2D, GLenum attachment);
    void attach(const Texture2DArray& texture2D, GLenum attachment, GLint layer);
    void attach(const Renderbuffer& texture2D, GLenum attachment);
    void setDrawBuffer(std::initializer_list<GLenum> attachment);
    void bind() const;
  private:
    GLuint ID;
  };
  class Shader
  {
  public:
    Shader(std::initializer_list<std::pair<std::string, GLenum> > shaderList);
    ~Shader();
    void use() const;
    void registerUniform(const std::string& name);

    void uniform(const std::string& name, const glm::mat4& value) const;
    void uniform(const std::string& name, const GLint& value) const;
    void uniform(const std::string& name, const GLfloat& value) const;
    void uniform(const std::string& name, const glm::vec3& value) const;

    void uniform(const std::string& name, GLsizei count, const glm::mat4* value) const;
    void uniform(const std::string& name, GLsizei count, const GLint* value) const;
    void uniform(const std::string& name, GLsizei count, const GLfloat* value) const;
    void uniform(const std::string& name, GLsizei count, const glm::vec3* value) const;

  //private:
    std::map<std::string, GLint> uniformLocations;
    GLuint ID;
  };
  class VertexBuffer
  {

  };
  class VertexArray
  {

  };
}
