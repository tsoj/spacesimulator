#pragma once

#include <glm/glm.hpp>

struct Position
{
  glm::vec3 coords;
};

struct Orientation
{
  glm::mat4 matrix;
};

struct Velocity
{
  glm::vec3 vector;
};

struct Light
{

};
