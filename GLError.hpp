#pragma once

#include <GL/glew.h>
#include <string>

namespace GLError
{
  void clearError()
  {
      while(glGetError() != GL_NO_ERROR);
    }

  void checkError(std::string s = "")
  {
    std::string message = s + ": OpenGL error:\n";
    GLenum error;
    error = glGetError();
    while(error != GL_NO_ERROR)
    {
      switch(error)
      {
        case GL_NO_ERROR:
        {
          throw std::runtime_error(message);
          break;
        }
        case GL_INVALID_ENUM:
        {
          message += "GL_INVALID_ENUM\n";
          break;
        }
        case GL_INVALID_VALUE:
        {
          message += "GL_INVALID_VALUE\n";
          break;
        }
        case GL_INVALID_OPERATION:
        {
          message += "GL_INVALID_OPERATION\n";
          break;
        }
        case GL_INVALID_FRAMEBUFFER_OPERATION:
        {
          message += "GL_INVALID_FRAMEBUFFER_OPERATION\n";
          break;
        }
        case GL_OUT_OF_MEMORY:
        {
          message += "GL_OUT_OF_MEMORY\n";
          break;
        }
        case GL_STACK_UNDERFLOW:
        {
          message += "GL_STACK_UNDERFLOW\n";
          break;
        }
        case GL_STACK_OVERFLOW:
        {
          message += "GL_STACK_OVERFLOW\n";
          break;
        }
      }
      error = glGetError();
    }
  }

  void checkFramebufferStatus(std::string s = "")
  {
    auto status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    if(status != GL_FRAMEBUFFER_COMPLETE)
    {
      std::string message = s + ": Framebuffer not complete:\n";

      switch(status)
      {
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        {
          message += "GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT\n";
          break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS_EXT:
        {
          message += "GL_FRAMEBUFFER_INCOMPLETE_DIMENSIONS\n";
          break;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        {
          message += "GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT\n";
          break;
        }
        case GL_FRAMEBUFFER_UNSUPPORTED:
        {
          message += "GL_FRAMEBUFFER_UNSUPPORTED\n";
          break;
        }
      }

      throw std::runtime_error(message);
    }
  }
}
