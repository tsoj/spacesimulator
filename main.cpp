#include <iostream>

#include "GameClass.hpp"

int main()
{
  std::cout << "Hello\n" << std::endl;

  GameClass gameClass = GameClass();
  gameClass.mainLoop();

  std::cout << "\nGood Bye" << std::endl;
  return 0;
}
