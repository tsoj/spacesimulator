#include "GameClass.hpp"
#include "Renderable.hpp"
#include "types.hpp"

#include <glm/gtc/matrix_transform.hpp>

GameClass::GameClass()
{
  {
    glfwSetErrorCallback(
      [](int error, const char* description)
      {
        throw std::runtime_error("Error: " + std::string(description) + " (" + std::to_string(error) + ")\n");
      }
    );

    if (not glfwInit())
    {
      throw std::runtime_error("Failed to initialize GLFW.\n");
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    window = glfwCreateWindow(1900, 950, "OpenglTest", NULL, NULL);
    if(not window)
    {
      glfwTerminate();
      throw std::runtime_error("Failed to initialize Window or context.\n");
    }
    glfwMakeContextCurrent(window);

    GLenum err = glewInit();
    if (GLEW_OK != err)
    {
      throw std::runtime_error("Failed to initialize GLEW: "+ std::string((char*)glewGetErrorString(err)) + "\n");
    }
    for(auto c : requiredExtensions)
    {
      if(not glewIsSupported(c))
      {
        std::string error = "Failed to find required extensions: ";
        error += c;
        error += "\n";
        throw std::runtime_error(error);
      }
    }


    glfwSwapInterval(1);

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glViewport(0, 0, width, height);

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_CULL_FACE);
    glCullFace(GL_FRONT);
    glFrontFace(GL_CW);

    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
  }

  systemManager.addSystem<RUNNING>(inputHandler, std::chrono::milliseconds(10));
  systemManager.addSystem<RUNNING>(renderer, std::chrono::milliseconds(30));
  systemManager.addSystem<RUNNING>(phyics, std::chrono::milliseconds(10));
  systemManager.initState<EXITING>(initExit);
  systemManager.initState<RUNNING>(initRunning);

  for(int i = 0; i<1; i++)
  {
    ecs::Entity spaceboat = entityManager.createEntity();
    spaceboat.createComponent<Position>(Position{glm::vec3(-5.0 + 0.01*i, 3.0, -10.0)});
    spaceboat.createComponent<Orientation>(Orientation{glm::mat4(1)});
    spaceboat.createComponent<Renderable>(Renderable("model/spaceboat.obj"));
    spaceboat.createComponent<Velocity>(Velocity{glm::vec3(0.0, 0.01*i, 0.0)});
    spaceboat.getComponent<Orientation>().matrix = glm::rotate(glm::mat4(1.0), glm::radians(100.0f), glm::vec3(1.0, 1.0, 0.0));
  }

  ecs::Entity monkeys = entityManager.createEntity();
  monkeys.createComponent<Position>(Position{glm::vec3(-5.0, -4.0, 0.0)});
  monkeys.createComponent<Orientation>(Orientation{glm::mat4(1)});
  monkeys.createComponent<Renderable>(Renderable("model/monkeys.obj"));
  monkeys.createComponent<Velocity>(Velocity{glm::vec3(1.0, 0.0, 0.0)});

  ecs::Entity plane = entityManager.createEntity();
  plane.createComponent<Position>(Position{glm::vec3(0.0, -5.0, -10.0)});
  plane.createComponent<Orientation>(Orientation{glm::mat4(1)});
  plane.createComponent<Renderable>(Renderable("model/hugePlane.obj"));
  plane.createComponent<Velocity>(Velocity{glm::vec3(0.0, 0.0, 0.0)});
}

GameClass::~GameClass()
{
  glfwTerminate();
}

void GameClass::phyics(GameClass &gameClass, const std::chrono::nanoseconds &deltaTime)
{
  glm::float32 deltaSeconds = ((double)deltaTime.count()) / 1000000000.0;
  for(auto entity : gameClass.entityManager.iterator<Velocity, Position>())
  {
    entity.getComponent<Position>().coords += entity.getComponent<Velocity>().vector * deltaSeconds;
  }
}

void GameClass::initRunning(UNUSED GameClass& gameClass)
{

}

void GameClass::initExit(GameClass &gameClass)
{
  gameClass.terminate();
}
