#include "GL.hpp"

#include <iostream>
#include <vector>
#include <utility>
#include <GL/glew.h>
#include <glm/glm.hpp>

#include "io.hpp"

namespace
{
  GLuint compileShader(std::initializer_list<std::pair<std::string, GLenum>> shaderList)
  {
  	GLuint programID = glCreateProgram();

  	std::vector<std::pair<std::string, GLuint>> shaderIDs = std::vector<std::pair<std::string, GLuint>>();

  	for(auto shader : shaderList)
  	{

  		GLuint shaderID = glCreateShader(shader.second);
  		shaderIDs.emplace_back(shader.first, shaderID);
  		std::string shaderCode = IO::readFile(shader.first);
  		const char* adapter = shaderCode.data();
  		glShaderSource(shaderID, 1, &adapter, 0);
  		glCompileShader(shaderID);
  	  glAttachShader(programID, shaderID);
  	}

    glLinkProgram(programID);

  	std::string error = "";

  	for(auto shader : shaderIDs)
  	{
  		GLint success = 0;
  	  glGetShaderiv(shader.second, GL_COMPILE_STATUS, &success);
  	  if(success == GL_FALSE)
  	  {
  	    GLint maxLength = 0;
  		  glGetShaderiv(shader.second, GL_INFO_LOG_LENGTH, &maxLength);
  	    std::vector<GLchar> errorLog(maxLength);

  	    glGetShaderInfoLog(shader.second, maxLength, &maxLength, &errorLog[0]);

  	    error += "Failed to compile " + shader.first + ":\n" + std::string(errorLog.data()) + "\n";
  	  }

  		glDetachShader(programID, shader.second);
  		glDeleteShader(shader.second);
  	}

  	GLint isLinked = 0;
  	glGetProgramiv(programID, GL_LINK_STATUS, (int*)&isLinked);
  	if(isLinked == GL_FALSE)
  	{
  		GLint maxLength = 0;
  		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);
  		std::vector<GLchar> errorLog(maxLength);

  		glGetProgramInfoLog(programID, maxLength, &maxLength, &errorLog[0]);

  		error += "Failed to link shader program:\n" + std::string(errorLog.data()) + "\n";
  	}

  	if(error != "")
  	{
  		glDeleteProgram(programID);
  		throw std::runtime_error("\n"+error);
  	}

  	return programID;
  }

  /*void glUniform1f(GLint location, GLfloat v0);

  void glUniform2f(GLint location, GLfloat v0, GLfloat v1);

  void glUniform3f(GLint location, GLfloat v0, G Lfloat v1, GLfloat v2);

  void glUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3);

  void glUniform1i(GLint location, GLint v0);

  void glUniform2i(GLint location, GLint v0, GLint v1);

  void glUniform3i(GLint location, GLint v0, GLint v1, GLint v2);

  void glUniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3);

  void glUniform1ui(GLint location, GLuint v0);

  void glUniform2ui(GLint location, GLuint v0, GLuint v1);

  void glUniform3ui(GLint location, GLuint v0, GLuint v1, GLuint v2);

  void glUniform4ui(GLint location, GLuint v0, GLuint v1, GLuint v2, GLuint v3);

  void glUniform1fv(GLint location, GLsizei count, const GLfloat *value);

  void glUniform2fv(GLint location, GLsizei count, const GLfloat *value);

  void glUniform3fv(GLint location, GLsizei count, const GLfloat *value);

  void glUniform4fv(GLint location, GLsizei count, const GLfloat *value);

  void glUniform1iv(GLint location, GLsizei count, const GLint *value);

  void glUniform2iv(GLint location, GLsizei count, const GLint *value);

  void glUniform3iv(GLint location, GLsizei count, const GLint *value);

  void glUniform4iv(GLint location, GLsizei count, const GLint *value);

  void glUniform1uiv(GLint location, GLsizei count, const GLuint *value);

  void glUniform2uiv(GLint location, GLsizei count, const GLuint *value);

  void glUniform3uiv(GLint location, GLsizei count, const GLuint *value);

  void glUniform4uiv(GLint location, GLsizei count, const GLuint *value);

  void glUniformMatrix2fv( GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix2x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix3x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix2x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix4x2fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix3x4fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

  void glUniformMatrix4x3fv(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);*/
}

namespace GL
{
  Shader::Shader(std::initializer_list<std::pair<std::string, GLenum> > shaderList)
  {
    ID = compileShader(shaderList);
  }
  Shader::~Shader()
  {
    glDeleteProgram(ID);
  }
  void Shader::use() const
  {
    glUseProgram(ID);
  }
  void Shader::registerUniform(const std::string& name)
  {
    uniformLocations[name] = glGetUniformLocation(ID, name.c_str());
  }

  void Shader::uniform(const std::string& name, const glm::mat4& value) const
  {
    glUniformMatrix4fv(uniformLocations.at(name), 1, GL_FALSE, &value[0][0]);
  }
  void Shader::uniform(const std::string& name, const GLint& value) const
  {
    glUniform1i(uniformLocations.at(name), value);
  }
  void Shader::uniform(const std::string& name, const GLfloat& value) const
  {
    glUniform1f(uniformLocations.at(name), value);
  }
  void Shader::uniform(const std::string& name, const glm::vec3& value) const
  {
    glUniform3fv(uniformLocations.at(name), 1, &value[0]);
  }
  void Shader::uniform(const std::string& name, GLsizei count, const glm::mat4* value) const
  {
    glUniformMatrix4fv(uniformLocations.at(name), count, GL_FALSE, &value[0][0][0]);
  }
  void Shader::uniform(const std::string& name, GLsizei count, const GLint* value) const
  {
    glUniform1iv(uniformLocations.at(name), count, &value[0]);
  }
  void Shader::uniform(const std::string& name, GLsizei count, const GLfloat* value) const
  {
    glUniform1fv(uniformLocations.at(name), count, &value[0]);
  }
  void Shader::uniform(const std::string& name, GLsizei count, const glm::vec3* value) const
  {
    glUniform3fv(uniformLocations.at(name), count, &value[0][0]);
  }
}
