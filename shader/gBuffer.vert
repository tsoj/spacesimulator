#version 330

layout(location = 0) in vec3 vertPosition_ModelSpace;
layout(location = 1) in vec3 vertNormal_ModelSpace;
layout(location = 2) in vec3 vertTangent_ModelSpace;
layout(location = 3) in vec2 textureCoordinate;

out VsOut
{
  vec3 fragPosition_WorldSpace;
  vec2 textureCoordinate;
  mat3 tangentToWorldSpace;
} vsOut;

uniform mat4 modelToWorld;
uniform mat4 worldToProjection;

void main()
{
  vec3 vertNormal_WorldSpace = normalize(vec3(modelToWorld * vec4(vertNormal_ModelSpace, 0.0)));
  vec3 vertTangent_WorldSpace = normalize(vec3(modelToWorld * vec4(vertTangent_ModelSpace, 0.0)));
  vec3 vertBiTangent_WorldSpace = normalize(cross(vertNormal_WorldSpace, vertTangent_WorldSpace));
  vsOut.tangentToWorldSpace = inverse(transpose(mat3(
        vertTangent_WorldSpace,
        vertBiTangent_WorldSpace,
        vertNormal_WorldSpace
    )));

  vec3 vertPosition_WorldSpace = vec3(modelToWorld * vec4(vertPosition_ModelSpace, 1.0));
  gl_Position = worldToProjection * vec4(vertPosition_WorldSpace, 1.0);
  vsOut.fragPosition_WorldSpace = vertPosition_WorldSpace;
  vsOut.textureCoordinate = textureCoordinate;
}
