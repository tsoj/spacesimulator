#version 330

layout (location = 0) out vec3 gPosition;
layout (location = 1) out vec3 gNormal;
layout (location = 2) out vec4 gDiffuse;

in VsOut
{
  vec3 fragPosition_WorldSpace;
  vec2 textureCoordinate;
  mat3 tangentToWorldSpace;
} vsOut;

uniform sampler2D diffuseTexture;
uniform sampler2D normalMap;

void main()
{
  gPosition = vsOut.fragPosition_WorldSpace;
  gNormal = vsOut.tangentToWorldSpace*normalize(texture2D(normalMap, vsOut.textureCoordinate).rgb * 2.0 - vec3(1.0, 1.0, 1.0));
  gDiffuse = texture2D(diffuseTexture, vsOut.textureCoordinate);
}
