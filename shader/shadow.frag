#version 330

const int NUM_CASCADES = 8;

layout (location = 0) out vec3 gShadow;

in VsOut
{
  vec4 fragPosition_LightSpace[NUM_CASCADES];
  vec4 fragPosition_WorldSpace;
} vsOut;

uniform sampler2DArrayShadow depthMap;
uniform float cascades[NUM_CASCADES];

float directionalShadowCalculation(
  vec4 fragPosition_LightSpace,
  sampler2DArrayShadow depthMap,
  int cascade
)
{
  vec3 projCoords = fragPosition_LightSpace.xyz / fragPosition_LightSpace.w;
  projCoords = projCoords * 0.5 + 0.5;
  float currentDepth = projCoords.z;
  return texture(depthMap, vec4(projCoords.xy, cascade, currentDepth));
}

void main()
{
  float shadow;

  int cascade;
  float currentCascadeDistance = length(vsOut.fragPosition_WorldSpace.xyz);
  for(int i = 0; i<NUM_CASCADES; i++)
  {
    cascade = i;
    if(currentCascadeDistance <= cascades[i])
    {
      break;
    }
  }

  shadow = directionalShadowCalculation(
    vsOut.fragPosition_LightSpace[cascade],
    depthMap,
    cascade
  );

  gShadow = vec3(1.0, 1.0, 1.0) * shadow;
}
