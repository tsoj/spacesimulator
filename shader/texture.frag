#version 330

out vec4 outColor;

in vec2 textureCoordinate;

uniform sampler2D texture;

void main()
{
  outColor = texture2D(texture, textureCoordinate);
}
