#version 330

out vec4 outColor;

in vec2 textureCoordinate;

uniform sampler2D position;
uniform sampler2D diffuse;
uniform sampler2D normal;
uniform sampler2D shadow;

uniform vec3 lightPosition_WorldSpace;

void main()
{
  vec3 position_WorldSpace = texture2D(position, textureCoordinate).xyz;
  vec3 normal_WorldSpace = texture2D(normal, textureCoordinate).xyz;
  vec4 diffuseColor = texture2D(diffuse, textureCoordinate).rgba;
  vec3 shadow = texture2D(shadow, textureCoordinate).rgb;

  vec3 toLight = normalize(lightPosition_WorldSpace - position_WorldSpace);
  float diffuseLight = clamp(dot(toLight, normal_WorldSpace), 0.0, 1.0);
  outColor = diffuseLight*diffuseColor*vec4(shadow, 1.0);
}
