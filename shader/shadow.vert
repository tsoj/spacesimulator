#version 330

const int NUM_CASCADES = 8;

layout(location = 0) in vec3 vertPosition_ModelSpace;
layout(location = 1) in vec3 vertNormal_ModelSpace;
layout(location = 2) in vec3 vertTangent_ModelSpace;
layout(location = 3) in vec2 textureCoordinate;

out VsOut
{
  vec4 fragPosition_LightSpace[NUM_CASCADES];
  vec4 fragPosition_WorldSpace;
} vsOut;

uniform mat4 modelToWorld;
uniform mat4 worldToProjection;
uniform mat4 worldToLight[NUM_CASCADES];

void main()
{

  vec3 vertPosition_WorldSpace = vec3(modelToWorld * vec4(vertPosition_ModelSpace, 1.0));
  gl_Position = worldToProjection * vec4(vertPosition_WorldSpace, 1.0);
  vsOut.fragPosition_WorldSpace = vec4(vertPosition_WorldSpace, 1.0);

  for(int i = 0; i<NUM_CASCADES; i++)
  {
    vsOut.fragPosition_LightSpace[i] = worldToLight[i] * vec4(vertPosition_WorldSpace, 1.0);
  }
}
