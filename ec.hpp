#pragma once
#include <iostream>
#include <vector>
#include <chrono>
#include <bitset>
#include <mutex>
#include <algorithm>
#include <cstring>

namespace ecs
{
  typedef size_t ID;
  const ID NULL_ID = 0-1; // == max_size(size_t);

  /********************ENTITIES and COMPONENTS********************/

  class EntityManager;

  template<typename A>
  class Entity
  {
    template<typename B, typename T, typename... Targs>
    friend class Iterator;
    friend class EntityManager;
    friend class Entity<const EntityManager>;

  public:

    void removeEntity();

    template<typename T, typename... Args>
    void createComponent(const Args&... args);

    template<typename T>
    void removeComponent();

    template<typename T>
    T & getComponent();

    template<typename T1, typename... Targs>
    bool hasComponents() const;

    Entity() : entityID(NULL_ID), entityManager(nullptr) {}
    ID getID()
    {
      return entityID;
    }

    inline bool operator==(const Entity<EntityManager>& rhs)
    {
      return this->entityID == rhs.entityID;
    }
    inline bool operator!=(const Entity<EntityManager>& rhs)
    {
      return not ((*this) == rhs);
    }

    /*Entity<const EntityManager> getConstEntity() // to make this function compilable the definition of Entity<const EntityManager> must
    { // be placed above the defintion of this class
      return Entity<const EntityManager>(entityManager, entityID);
    }*/

  private:

    Entity(A& entityManager, ID entityID) : entityID(entityID), entityManager(&entityManager) {}
    ID entityID;
    EntityManager* entityManager;
  };

  template<>
  class Entity<const EntityManager>
  {
    template<typename A, typename T, typename... Targs>
    friend class Iterator;
    friend class EntityManager;
    friend class Entity<EntityManager>;

  public:

    template<typename T>
    const T & getComponent() const;

    template<typename T1, typename... Targs>
    bool hasComponents() const;

    Entity() : entityID(NULL_ID), entityManager(nullptr) {}
    ID getID()
    {
      return entityID;
    }

    inline bool operator==(const Entity<const EntityManager>& rhs)
    {
      return this->entityID == rhs.entityID;
    }
    inline bool operator!=(const Entity<const EntityManager>& rhs)
    {
      return not ((*this) == rhs);
    }

    Entity<EntityManager> getMutableEntity(EntityManager& entityManager)
    {
      return Entity<EntityManager>(entityManager, entityID);
    }

  private:

    Entity(const EntityManager& entityManager, ID entityID) : entityID(entityID), entityManager(&entityManager) {}
    ID entityID;
    const EntityManager* entityManager;
  };

  class EntityManager
  {
    template<typename T>
    friend class Entity;

    template<typename T>
    class ComponentContainer;

    template<typename T, typename... Targs>
    class Iterator;

    template<typename T, typename... Targs>
    class ConstIterator;

  public:

    EntityManager()
    {
      if(numberInstances != 0)
      {
        throw std::runtime_error("Only one EntityManager instance can exist at one time.");
      }
      else
      {
        numberInstances += 1;
      }
    }
    ~EntityManager()
    {
      reset();
      numberInstances -= 1;
    }
    EntityManager(EntityManager const&) = delete;
    void operator=(EntityManager const&) = delete;

    inline void reset();

    template<typename T, typename... Targs>
    ConstIterator<T, Targs...> iterator() const;

    template<typename T, typename... Targs>
    Iterator<T, Targs...> iterator();

    template<typename T, typename... Targs>
    ConstIterator<T, Targs...> iterator(Entity<const EntityManager> entity) const;

    template<typename T, typename... Targs>
    Iterator<T, Targs...> iterator(Entity<EntityManager> entity);

    inline Entity<EntityManager> createEntity();

  private:

    inline static int numberInstances = 0;

    static const size_t SIZE_BIT_MASK = 64;
    inline static std::bitset<SIZE_BIT_MASK> getUniqueMask()
    {
      static std::bitset<SIZE_BIT_MASK> counter = std::bitset<SIZE_BIT_MASK>(0b10);
      std::bitset<SIZE_BIT_MASK> tmp = counter;
      counter <<= 1;
      if(counter.none())
      {
        throw std::runtime_error("Too many component types.");
      }
      return tmp;
    }

    template<typename T>
    inline static std::bitset<SIZE_BIT_MASK> getBitIDUnion()
    {
      static const std::bitset<SIZE_BIT_MASK> uniqueBitID = getUniqueMask();
      return uniqueBitID;
    }
    template<typename T1, typename T2, typename... Targs>
    inline static std::bitset<SIZE_BIT_MASK> getBitIDUnion()
    {
      return getBitIDUnion<T1>() | getBitIDUnion<T2, Targs...>();
    }
    template<typename T1, typename... Targs>
    inline const static std::bitset<SIZE_BIT_MASK> bitID = getBitIDUnion<T1, Targs...>();

    inline void removeEntity(ID entityID);

    template<typename T, typename... Args>
    inline void createComponent(ID entityID, const Args&... args);

    template<typename T>
    inline void removeComponent(ID entityID);

    template<typename T>
    inline T & getComponent(ID entityID);

    template<typename T>
    inline const T & getConstComponent(ID entityID) const;

    template<typename T1, typename... Targs>
    inline bool hasComponents(ID entityID) const;

    inline static std::vector<ID> freeEntityIDs = std::vector<ID>();
    inline static std::vector<std::bitset<SIZE_BIT_MASK>> bitIDs = std::vector<std::bitset<SIZE_BIT_MASK>>();
    inline static std::vector<std::bitset<SIZE_BIT_MASK>> constructedComponents = std::vector<std::bitset<SIZE_BIT_MASK>>();

    template<typename T>
    inline static ComponentContainer<T> components = ComponentContainer<T>();
  };


  template<typename T>
  class EntityManager::ComponentContainer
  {
    T* buffer;
    size_t capacity;
    size_t m_size;

  public:

    ComponentContainer();
    ~ComponentContainer();
    size_t size();
    void resize(size_t size);
    T & operator[](size_t index);
  };
  template<typename T>
  EntityManager::ComponentContainer<T>::ComponentContainer()
  {
    capacity = 1;
    buffer = (T*)malloc(capacity*sizeof(T));
    m_size = 0;
  }
  template<typename T>
  EntityManager::ComponentContainer<T>::~ComponentContainer()
  {
    for(size_t i = 0; i<constructedComponents.size(); i++)
    {
      if((bitID<T> & constructedComponents[i]).any())
      {
        buffer[i].~T();
      }
    }
    free(buffer);
  }
  template<typename T>
  size_t EntityManager::ComponentContainer<T>::size()
  {
    return m_size;
  }
  template<typename T>
  void EntityManager::ComponentContainer<T>::resize(size_t newSize)
  {
    if(this->m_size >= newSize)
    {
      this->m_size = newSize;
    }
    else
    {
      if(this->capacity < newSize)
      {
        const size_t newCapacity = capacity*2;
        T* newBuffer = (T*)malloc(newCapacity*sizeof(T));
        for(size_t i = 0; i<m_size; i++)
        {
          if((bitID<T> & constructedComponents[i]).any())
          {
            new (&newBuffer[i]) T(this->buffer[i]);
          }
        }
        this->capacity = newCapacity;
        free(this->buffer);
        this->buffer = newBuffer;
      }
      this->m_size = newSize;
    }

  }
  template<typename T>
  T & EntityManager::ComponentContainer<T>::operator[](size_t index)
  {
    return buffer[index];
  }


  inline void EntityManager::reset()
  {
    freeEntityIDs.clear();
    bitIDs.clear();
  }

  Entity<EntityManager> EntityManager::createEntity()
  {
    ID newID = NULL_ID;
    if(freeEntityIDs.empty())
    {
      newID = bitIDs.size();
      bitIDs.emplace_back(1);
    }
    else
    {
      newID = freeEntityIDs.back();
      freeEntityIDs.pop_back();
      bitIDs[newID] = std::bitset<SIZE_BIT_MASK>(1);
    }
    if(newID >= constructedComponents.size())
    {
      constructedComponents.emplace_back(0);
    }
    return Entity(*this, newID);
  }
  void EntityManager::removeEntity(ID entityID)
  {
    bitIDs[entityID] = std::bitset<SIZE_BIT_MASK>(0);
    freeEntityIDs.emplace_back(entityID);
  }
  template<typename T, typename... Args>
  void EntityManager::createComponent(ID entityID, const Args&... args)
  {
    if(entityID >= components<T>.size())
    {
      components<T>.resize(entityID+1);
    }

    if((constructedComponents[entityID] & bitID<T>).any())
    {
      components<T>[entityID].~T();
    }
    else
    {
      constructedComponents[entityID] |= bitID<T>;
    }

    bitIDs[entityID] |= bitID<T>;
    new (&components<T>[entityID]) T(args...);
  }
  template<typename T>
  void EntityManager::removeComponent(ID entityID)
  {
    if(not hasComponents<T>(entityID))
    {
      throw std::runtime_error("Tried to remove non-existing component.");
    }
    components<T>[entityID].~T();
    constructedComponents[entityID] &= ~bitID<T>;
    bitIDs[entityID] &= ~bitID<T>;
  }
  template<typename T>
  T & EntityManager::getComponent(ID entityID)
  {
    if(not hasComponents<T>(entityID))
    {
      throw std::runtime_error("Tried to access non-existing component.");
    }
    return components<T>[entityID];
  }
  template<typename T>
  const T & EntityManager::getConstComponent(ID entityID) const
  {
    if(not hasComponents<T>(entityID))
    {
      throw std::runtime_error("Tried to access non-existing component.");
    }
    return components<T>[entityID];
  }
  template<typename T1, typename... Targs>
  bool EntityManager::hasComponents(ID entityID) const
  {
    if(entityID >= bitIDs.size())
    {
      return false;
    }
    return (bitIDs[entityID] & bitID<T1, Targs...>) == bitID<T1, Targs...>;
  }
  template<>
  inline bool EntityManager::hasComponents<void>(ID entityID) const
  {
    if(entityID >= bitIDs.size())
    {
      return false;
    }
    return bitIDs[entityID].test(0);
  }


  template<typename A>
  void Entity<A>::removeEntity()
  {
    this->entityManager->removeEntity(this->entityID);
  }
  template<typename A>
  template<typename T, typename... Args>
  void Entity<A>::createComponent(const Args&... args)
  {
    this->entityManager->template createComponent<T>(this->entityID, args...);
  }
  template<typename A>
  template<typename T>
  void Entity<A>::removeComponent()
  {
    this->entityManager->template removeComponent<T>(this->entityID);
  }
  template<typename A>
  template<typename T>
  T & Entity<A>::getComponent()
  {
    return this->entityManager->template getComponent<T>(this->entityID);
  }
  template<typename A>
  template<typename T1, typename... Targs>
  bool Entity<A>::hasComponents() const
  {
    return this->entityManager->template hasComponents<T1, Targs...>(this->entityID);
  }

  template<typename T>
  const T & Entity<const EntityManager>::getComponent() const
  {
    return this->entityManager->getConstComponent<T>(this->entityID);
  }
  template<typename T1, typename... Targs>
  bool Entity<const EntityManager>::hasComponents() const
  {
    return this->entityManager->hasComponents<T1, Targs...>(this->entityID);
  }


  template<typename T, typename... Targs>
  class EntityManager::Iterator
  {
  private:

    ID entityID;
    EntityManager& entityManager;

    ID findNextValidEntity()
    {
      ID i = entityID;
      while(end().entityID > i)
      {
        if(entityManager.hasComponents<T, Targs...>(i))
        {
          return i;
        }
        i+=1;
      }
      return end().entityID;
    }

  public:

    Iterator(EntityManager& entityManager) : entityID(0), entityManager(entityManager) {}
    explicit Iterator(EntityManager& entityManager, ID entityID) : entityID(entityID), entityManager(entityManager) {}
    explicit Iterator(EntityManager& entityManager, Entity<EntityManager> entity) : entityID(entity.entityID), entityManager(entityManager) {}

    Iterator<T, Targs...> begin();
    Iterator<T, Targs...> end();

    void operator= (const Iterator<T, Targs...>& a);
    bool operator== (const Iterator<T, Targs...>& a);
    bool operator!= (const Iterator<T, Targs...>& a);
    Entity<EntityManager> operator*();
    Entity<EntityManager> operator->();
    Iterator<T, Targs...> operator++();
  };

  template<typename T, typename... Targs>
  EntityManager::Iterator<T, Targs...> EntityManager::Iterator<T, Targs...>::begin()
  {
    return Iterator<T, Targs...>(entityManager, findNextValidEntity());
  }
  template<typename T, typename... Targs>
  EntityManager::Iterator<T, Targs...> EntityManager::Iterator<T, Targs...>::end()
  {
    return Iterator<T, Targs...>(entityManager, EntityManager::bitIDs.size());
  }
  template<typename T, typename... Targs>
  void EntityManager::Iterator<T, Targs...>::operator= (const EntityManager::Iterator<T, Targs...>& a)
  {
    entityID = a.entityID;
    entityManager = a.entityManager;
  }
  template<typename T, typename... Targs>
  bool EntityManager::Iterator<T, Targs...>::operator== (const EntityManager::Iterator<T, Targs...>& a)
  {
    return entityID == a.entityID && &entityManager == &a.entityManager;
  }
  template<typename T, typename... Targs>
  bool EntityManager::Iterator<T, Targs...>::operator!= (const EntityManager::Iterator<T, Targs...>& a)
  {
    return entityID != a.entityID || &entityManager != &a.entityManager;
  }
  template<typename T, typename... Targs>
  Entity<EntityManager> EntityManager::Iterator<T, Targs...>::operator*()
  {
    return Entity(entityManager, entityID);
  }
  template<typename T, typename... Targs>
  Entity<EntityManager> EntityManager::Iterator<T, Targs...>::operator->()
  {
    return Entity(entityManager, entityID);
  }
  template<typename T, typename... Targs>
  EntityManager::Iterator<T, Targs...> EntityManager::Iterator<T, Targs...>::operator++() // Prefix Increment
  {
    entityID += 1;
    entityID = findNextValidEntity();
    return Iterator<T, Targs...>(entityManager, entityID);
  }


  template<typename T, typename... Targs>
  class EntityManager::ConstIterator
  {
  private:

    ID entityID;
    const EntityManager& entityManager;

    ID findNextValidEntity()
    {
      ID i = entityID;
      while(end().entityID > i)
      {
        if(entityManager.hasComponents<T, Targs...>(i))
        {
          return i;
        }
        i+=1;
      }
      return end().entityID;
    }

  public:

    ConstIterator(const EntityManager& entityManager) : entityID(0), entityManager(entityManager) {}
    explicit ConstIterator(const EntityManager& entityManager, ID entityID) : entityID(entityID), entityManager(entityManager) {}
    explicit ConstIterator(const EntityManager& entityManager, Entity<const EntityManager> entity) : entityID(entity.entityID), entityManager(entityManager) {}

    ConstIterator<T, Targs...> begin();
    ConstIterator<T, Targs...> end();

    void operator= (const ConstIterator<T, Targs...>& a);
    bool operator== (const ConstIterator<T, Targs...>& a);
    bool operator!= (const ConstIterator<T, Targs...>& a);
    Entity<const EntityManager> operator*();
    Entity<const EntityManager> operator->();
    ConstIterator<T, Targs...> operator++();
  };

  template<typename T, typename... Targs>
  EntityManager::ConstIterator<T, Targs...> EntityManager::ConstIterator<T, Targs...>::begin()
  {
    return ConstIterator<T, Targs...>(entityManager, findNextValidEntity());
  }
  template<typename T, typename... Targs>
  EntityManager::ConstIterator<T, Targs...> EntityManager::ConstIterator<T, Targs...>::end()
  {
    return ConstIterator<T, Targs...>(entityManager, EntityManager::bitIDs.size());
  }
  template<typename T, typename... Targs>
  void EntityManager::ConstIterator<T, Targs...>::operator= (const EntityManager::ConstIterator<T, Targs...>& a)
  {
    entityID = a.entityID;
    entityManager = a.entityManager;
  }
  template<typename T, typename... Targs>
  bool EntityManager::ConstIterator<T, Targs...>::operator== (const EntityManager::ConstIterator<T, Targs...>& a)
  {
    return entityID == a.entityID && &entityManager == &a.entityManager;
  }
  template<typename T, typename... Targs>
  bool EntityManager::ConstIterator<T, Targs...>::operator!= (const EntityManager::ConstIterator<T, Targs...>& a)
  {
    return entityID != a.entityID || &entityManager != &a.entityManager;
  }
  template<typename T, typename... Targs>
  Entity<const EntityManager> EntityManager::ConstIterator<T, Targs...>::operator*()
  {
    return Entity(entityManager, entityID);
  }
  template<typename T, typename... Targs>
  Entity<const EntityManager> EntityManager::ConstIterator<T, Targs...>::operator->()
  {
    return Entity(entityManager, entityID);
  }
  template<typename T, typename... Targs>
  EntityManager::ConstIterator<T, Targs...> EntityManager::ConstIterator<T, Targs...>::operator++() // Prefix Increment
  {
    entityID += 1;
    entityID = findNextValidEntity();
    return ConstIterator<T, Targs...>(entityManager, entityID);
  }


  template<typename T, typename... Targs>
  EntityManager::ConstIterator<T, Targs...> EntityManager::iterator() const
  {
    return ConstIterator<T, Targs...>(*this);
  }
  template<typename T, typename... Targs>
  EntityManager::Iterator<T, Targs...> EntityManager::iterator()
  {
    return Iterator<T, Targs...>(*this);
  }
  template<typename T, typename... Targs>
  EntityManager::ConstIterator<T, Targs...> EntityManager::iterator(Entity<const EntityManager> entity) const
  {
    return ConstIterator<T, Targs...>(*this, entity);
  }
  template<typename T, typename... Targs>
  EntityManager::Iterator<T, Targs...> EntityManager::iterator(Entity<EntityManager> entity)
  {
    return Iterator<EntityManager, T, Targs...>(*this, entity);
  }
}

/*MIT License

Copyright (c) 2018 tsoj

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/
