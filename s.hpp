#pragma once
#include <iostream>
#include <vector>
#include <chrono>
#include <bitset>
#include <mutex>
#include <algorithm>

#ifdef OMP
#include <omp.h>
#endif

#include "util.hpp"

#define UNUSED __attribute__((unused))

namespace ecs
{
  typedef std::chrono::nanoseconds Duration;
  typedef std::chrono::time_point<std::chrono::high_resolution_clock> TimePoint;
  typedef std::chrono::high_resolution_clock Clock;
  typedef int State;

  /********************SYSTEMS********************/

  template<typename A>
  class SystemManager // TODO: define behaviour if a new state gets entered but another new state has already been entered in this frame
  {
  public:

    SystemManager(A& a) : a(a)
    {
      if(numberInstances != 0)
      {
        throw std::runtime_error("Only one SystemManager instance can exist at one time.");
      }
      else
      {
        numberInstances += 1;
      }
    }
    ~SystemManager()
    {
      reset();
      numberInstances -= 1;
    }

    template<State S>
    void addSystem(void (*update)(const A&, const Duration& deltaTime), const Duration& callDeltaTime)
    {
      //registrate time based system state
      STATIC_CALL(registrateTimeBasedSystemState<S>());

      constTimeBasedSystems<S>.emplace_back(update, callDeltaTime);
    }

    template<State S1, State S2, State... Sargs>
    void addSystem(void (*update)(const A&, const Duration& deltaTime), const Duration& callDeltaTime)
    {
      addSystem<S1>(update, callDeltaTime);
      addSystem<S2, Sargs...>(update, callDeltaTime);
    }

    template<State S>
    void addSystem(void (*update)(A&, const Duration& deltaTime), const Duration& callDeltaTime)
    {
      //registrate time based system state
      STATIC_CALL(registrateTimeBasedSystemState<S>());

      timeBasedSystems<S>.emplace_back(update, callDeltaTime);
    }

    template<State S1, State S2, State... Sargs>
    void addSystem(void (*update)(A&, const Duration& deltaTime), const Duration& callDeltaTime)
    {
      addSystem<S1>(update, callDeltaTime);
      addSystem<S2, Sargs...>(update, callDeltaTime);
    }

    template<typename T, State S>
    void addSystem(void (*update)(const T&, const A&))
    {
      //registrate event type
      STATIC_CALL((registrateEvent<T, S>()));

      constEventBasedSystem<T, S>.push_back(update);
    }

    template<typename T, State S1, State S2, State... Sargs>
    void addSystem(void (*update)(const T&, const A&))
    {
      addSystem<T, S1>(update);
      addSystem<T, S2, Sargs...>(update);
    }

    template<typename T, State S>
    void addSystem(void (*update)(const T&, A&))
    {
      //registrate event type
      STATIC_CALL((registrateEvent<T, S>()));

      eventBasedSystem<T, S>.push_back(update);
    }

    template<typename T, State S1, State S2, State... Sargs>
    void addSystem(void (*update)(const T&, A&))
    {
      addSystem<T, S1>(update);
      addSystem<T, S2, Sargs...>(update);
    }

    void runSystems()
    {
      if(currentAtEnterState != nullptr)
      {
        currentAtEnterState(a);
        currentAtEnterState = nullptr;
      }
      if(currentRunSystems != nullptr)
      {
        currentRunSystems(a);
      }
    }

    template<typename T>
    void throwEvent(const T& event) const
    {
      static std::mutex mutex;
      std::lock_guard<std::mutex> lock(mutex);

      eventQueue<T>.push_back(event);
    }

    template<State S>
    void initState(void (*initState)(A&))
    {
      initStateFunction<S> = initState;
    }


    template<State S>
    void enterState() const
    {
      static std::mutex mutex;
      std::lock_guard<std::mutex> lock(mutex);

      currentAtEnterState = atEnterState<S>;
    }

    void reset()
    {
      for(auto f : resetVectorsList)
      {
        f();
      }
    }

  private:

    inline static int numberInstances = 0;

    A& a;

    inline static void (*currentRunSystems)(A&) = nullptr;
    inline static void (*currentAtEnterState)(A&) = nullptr;

    template<State S>
    inline static void (*initStateFunction)(A&) = nullptr;

    struct TimeBasedSystem
    {
      TimeBasedSystem(void (*update)(A&, const Duration& deltaTime), Duration callDeltaTime) :
      update(update),
      callDeltaTime(callDeltaTime),
      lastUpdateCallTime(TimePoint())
      {
      }
      void (*update)(A&, const Duration& deltaTime);
      Duration callDeltaTime;
      TimePoint lastUpdateCallTime;
    };
    struct ConstTimeBasedSystem
    {
      ConstTimeBasedSystem(void (*update)(const A&, const Duration& deltaTime), Duration callDeltaTime) :
      update(update),
      callDeltaTime(callDeltaTime),
      lastUpdateCallTime(TimePoint())
      {
      }
      void (*update)(const A&, const Duration& deltaTime);
      Duration callDeltaTime;
      TimePoint lastUpdateCallTime;
    };

    template<State S>
    inline static std::vector<ConstTimeBasedSystem> constTimeBasedSystems = std::vector<ConstTimeBasedSystem>();

    template<State S>
    inline static std::vector<TimeBasedSystem> timeBasedSystems = std::vector<TimeBasedSystem>();

    template<typename T, State S>
    inline static std::vector<void(*)(const T&, const A&)> constEventBasedSystem = std::vector<void(*)(const T&, const A&)>();

    template<typename T, State S>
    inline static std::vector<void(*)(const T&, A&)> eventBasedSystem = std::vector<void(*)(const T&, A&)>();

    template<typename T>
    inline static std::vector<T> eventQueue = std::vector<T>();

    template<State S>
    static inline void atEnterState(A& a)
    {
      currentRunSystems = runStateSystems<S>;

      if(initStateFunction<S> != nullptr)
      {
        initStateFunction<S>(a);
      }

      TimePoint now = Clock::now();
      for(ConstTimeBasedSystem & system : constTimeBasedSystems<S>)
      {
        system.lastUpdateCallTime = now;
      }
      for(TimeBasedSystem & system : timeBasedSystems<S>)
      {
        system.lastUpdateCallTime = now;
      }
    }

    template<State S>
    static inline void runStateSystems(A& a)
    {
      #ifdef OMP
      #pragma omp parallel for schedule(dynamic)
      #endif
      for(size_t i = 0; i<constTimeBasedSystems<S>.size(); i++)
      {
        ConstTimeBasedSystem& system = constTimeBasedSystems<S>[i];
        TimePoint now = Clock::now();
        Duration deltaTime = now - system.lastUpdateCallTime;
        if(system.callDeltaTime <= deltaTime)
        {
          system.update(a, deltaTime);
          system.lastUpdateCallTime = now;
        }
      }

      for(TimeBasedSystem & system : timeBasedSystems<S>)
      {
        TimePoint now = Clock::now();
        Duration deltaTime = now - system.lastUpdateCallTime;
        if(system.callDeltaTime <= deltaTime)
        {
          system.update(a, deltaTime);
          system.lastUpdateCallTime = now;
        }
      }
      for(auto f : runEventBasedSystemsList<S>)
      {
        f(a);
      }
    }

    template<State S>
    inline static std::vector<void(*)(A& a)> runEventBasedSystemsList = std::vector<void(*)(A& a)>();

    template<typename T, State S>
    static void runEventBasedSystems(A& a)
    {
      for(const T & event : eventQueue<T>)
      {
        #ifdef OMP
        #pragma omp parallel for schedule(dynamic)
        #endif
        for(size_t i = 0; i<constEventBasedSystem<T, S>.size(); i++)
        {
          auto f = constEventBasedSystem<T, S>[i];
          f(event, a);
        }

        for(auto f : eventBasedSystem<T, S>)
        {
          f(event, a);
        }
      }
      eventQueue<T>.clear();
    }

    inline static std::vector<void(*)()> resetVectorsList = std::vector<void(*)()>();
    template<typename T, State S>
    static void resetEventVectors()
    {
      constEventBasedSystem<T, S> = std::vector<void(*)(const T&, const A&)>();
      eventBasedSystem<T, S> = std::vector<void(*)(const T&, A&)>();
      eventQueue<T> = std::vector<T>();
    }
    template<State S>
    static void resetTimeBasedVectors()
    {
      constTimeBasedSystems<S> = std::vector<ConstTimeBasedSystem>();
      timeBasedSystems<S> = std::vector<TimeBasedSystem>();
    }

    template<typename T, State S>
    static void registrateEvent()
    {
      if(
        std::find(
          resetVectorsList.begin(),
          resetVectorsList.end(),
          resetEventVectors<T, S>
        ) == resetVectorsList.end()
      )
      {
        resetVectorsList.push_back(resetEventVectors<T, S>);
        runEventBasedSystemsList<S>.push_back(runEventBasedSystems<T, S>);
      }
    }

    template<State S>
    static void registrateTimeBasedSystemState()
    {
      if(
        std::find(
          resetVectorsList.begin(),
          resetVectorsList.end(),
          resetTimeBasedVectors<S>
        ) == resetVectorsList.end()
      )
      {
        resetVectorsList.push_back(resetTimeBasedVectors<S>);
      }
    }
  };
}

/*MIT License

Copyright (c) 2018 tsoj

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.*/
