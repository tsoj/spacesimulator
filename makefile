CC = clang++

OPTIMIZATION = -O2

CFLAGS = -std=c++17 $(OPTIMIZATION) -Wall -Wextra -Wpedantic -fopenmp -fno-omit-frame-pointer -g#-DOMP
LDFLAGS = -std=c++17 $(OPTIMIZATION) -Wall -Wextra -Wpedantic -lGLEW -lGLU -lGL -lglfw -lX11 -lXrandr -lXxf86vm -pthread -ldl -fopenmp -fno-omit-frame-pointer -g

NAME = spacesimulator
BIN_FILE_PATH = ./bin/

CPP = main.cpp GameClass.cpp inputHandler.cpp Texture.cpp lodepng.cpp renderer.cpp GL_Texture.cpp GL_Framebuffer.cpp GL_Shader.cpp

OBJ = $(CPP:%.cpp=$(BIN_FILE_PATH)%.o)

-include $(OBJ:%.o=%.d)

all: $(OBJ)
	$(CC) -o $(BIN_FILE_PATH)$(NAME) $(OBJ) $(LDFLAGS)

$(BIN_FILE_PATH)%.o: %.cpp
	$(CC) $(CFLAGS) -c $*.cpp -o $(BIN_FILE_PATH)$*.o
	echo -n $(BIN_FILE_PATH) > $(BIN_FILE_PATH)$*.d
	$(CC) $(CFLAGS) -MM $*.cpp >> $(BIN_FILE_PATH)$*.d

test: all
	$(BIN_FILE_PATH)$(NAME)

clean:
	rm $(BIN_FILE_PATH)*.d $(BIN_FILE_PATH)*.o
