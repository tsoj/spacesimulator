#pragma once

#include <vector>
#include <map>

#include "Vertex.hpp"
#include "Obj.hpp"
#include "Texture.hpp"

struct StaticMesh
{
  StaticMesh(){}
  StaticMesh(const Obj::O& o)
  {
    this->ambientTextureID = Texture(o.mtl.ambientTexture.c_str()).loadGlTexture();
    this->diffuseTextureID = Texture(o.mtl.diffuseTexture.c_str()).loadGlTexture();
    this->specularColorTextureID = Texture(o.mtl.specularColorTexture.c_str()).loadGlTexture();
    this->specularHighlightTextureID = Texture(o.mtl.specularHighlightTexture.c_str()).loadGlTexture();
    this->normalMapID = Texture(o.mtl.normalMap.c_str()).loadGlTexture();

    this->vertices = o.vertices;

    glGenVertexArrays(1, &this->vertexArrayID);
    glBindVertexArray(this->vertexArrayID);

    glGenBuffers(1, &this->vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, this->vertexBufferID);

    glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * this->vertices.size() , this->vertices.data(), GL_STATIC_DRAW);

    glEnableVertexAttribArray(0);
    glVertexAttribPointer(
      0,                  											// attribute
      3,     																		// size
      GL_FLOAT,          												// type
      GL_FALSE,																	// normalized
      sizeof(Vertex),														// stride
      (void*)offsetof(Vertex, position)					// array buffer offset
    );
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(
      1,                  											// attribute
      3,     																		// size
      GL_FLOAT,          												// type
      GL_FALSE,																	// normalized
      sizeof(Vertex),														// stride
      (void*)offsetof(Vertex, normal)						// array buffer offset
    );
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(
      2,                  											// attribute
      3,     																		// size
      GL_FLOAT,          												// type
      GL_FALSE,																	// normalized
      sizeof(Vertex),														// stride
      (void*)offsetof(Vertex, tangent)					// array buffer offset
    );
    glEnableVertexAttribArray(3);
    glVertexAttribPointer(
      3,                  											// attribute
      2,     																		// size
      GL_FLOAT,          												// type
      GL_FALSE,																	// normalized
      sizeof(Vertex),														// stride
      (void*)offsetof(Vertex, textureCoordinate)// array buffer offset
    );
  }

  GLuint getDiffuseTextureID()
  {
    return diffuseTextureID;
  }
  GLuint getNormalMapID()
  {
    return normalMapID;
  }
  size_t getNumVertices()
  {
    return vertices.size();
  }
  GLuint getVertexArrayID()
  {
    return vertexArrayID;
  }

private:
  GLuint ambientTextureID;
  GLuint diffuseTextureID;
  GLuint specularColorTextureID;
  GLuint specularHighlightTextureID;
  GLuint normalMapID;

  std::vector<Vertex> vertices = std::vector<Vertex>();

  GLuint vertexBufferID;
  GLuint vertexArrayID;
};

struct Renderable
{
  Renderable(std::string objFile)
  {
    Obj obj = Obj(objFile);
    for(auto o : obj.objects)
    {
      staticMeshs.emplace_back(o);
    }
  }

  std::vector<StaticMesh> staticMeshs;
};
