#include "GL.hpp"

#include <iostream>

namespace GL
{
  Framebuffer::Framebuffer()
  {
    glGenFramebuffers(1, &ID);
    bind();
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
  }
  Framebuffer::~Framebuffer()
  {
    glDeleteFramebuffers(1, &ID);
  }

  void Framebuffer::attach(const Texture2D& texture2D, GLenum attachment)
  {
    bind();
    glFramebufferTexture2D(GL_FRAMEBUFFER, attachment, GL_TEXTURE_2D, texture2D.ID, 0);
  }

  void Framebuffer::attach(const Texture2DArray& texture2DArray, GLenum attachment, GLint layer)
  {
    bind();
    glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, texture2DArray.ID, 0, layer);
  }

  void Framebuffer::attach(const Renderbuffer& renderbuffer, GLenum attachment)
  {
    bind();
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, attachment, GL_RENDERBUFFER, renderbuffer.ID);
  }

  void Framebuffer::setDrawBuffer(std::initializer_list<GLenum> attachment)
  {
    bind();
    glDrawBuffers(attachment.size(), attachment.begin());
  }

  void Framebuffer::bind() const
  {
    glBindFramebuffer(GL_FRAMEBUFFER, ID);
  }
}
