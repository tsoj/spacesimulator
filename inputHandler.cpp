#include "GameClass.hpp"

void GameClass::inputHandler(const GameClass& gameClass, UNUSED const std::chrono::nanoseconds& dt)
{
  glfwPollEvents();
  if(glfwWindowShouldClose(gameClass.window))
  {
    gameClass.systemManager.enterState<EXITING>();
  }
}
