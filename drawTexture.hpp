#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>

#include "compileShader.hpp"
#include "GL.hpp"

inline void drawTexture(GLFWwindow* window, const GL::Texture2D& texture)
{
  const static GLuint programID = compileShader({{"shader/texture.vert", GL_VERTEX_SHADER},{"shader/texture.frag", GL_FRAGMENT_SHADER}});
  const static GLint texture_UniformLocation = glGetUniformLocation(programID, "texture");
  const static glm::vec2 verts[] =
	{
	  glm::vec2(-1.0, 1.0),glm::vec2(0.0, 1.0),  glm::vec2(-1.0, -1.0),glm::vec2(0.0, 0.0),  glm::vec2(1.0, 1.0),glm::vec2(1.0, 1.0),
	  glm::vec2(1.0, -1.0),glm::vec2(1.0, 0.0),  glm::vec2(1.0, 1.0),glm::vec2(1.0, 1.0),  glm::vec2(-1.0, -1.0),glm::vec2(0.0, 0.0)
    // vertexcoords     ,texturecoords
	};
  static GLuint vertexBufferID;
  static GLuint vertexArrayID;
  static auto init = []()->char
  {
    glGenVertexArrays(1, &vertexArrayID);
    glBindVertexArray(vertexArrayID);

    glGenBuffers(1, &vertexBufferID);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);

    glBufferData(GL_ARRAY_BUFFER, sizeof(verts) , verts, GL_STATIC_DRAW);
  	glEnableVertexAttribArray(0);
  	glVertexAttribPointer(
  		0,                  											// attribute 0. No particular reason for 0, but must match the layout in the shader.
  		2,     																		// size
  		GL_FLOAT,          												// type
  		GL_FALSE,																	// normalized
  		sizeof(glm::vec2)*2,											 // stride
  		(void*)0            											// array buffer offset
  	);
  	glEnableVertexAttribArray(1);
  	glVertexAttribPointer(
  		1,                  											// attribute 0. No particular reason for 0, but must match the layout in the shader.
  		2,     																		// size
  		GL_FLOAT,          												// type
  		GL_FALSE,																	// normalized
  		sizeof(glm::vec2)*2,											// stride
  		(void*)sizeof(glm::vec2)            			// array buffer offset
  	);

    return 0;
  };
  #pragma GCC diagnostic push
  #pragma GCC diagnostic ignored "-Wunused-variable"
  const static char _ = init();
  #pragma GCC diagnostic pop


  glBindFramebuffer(GL_FRAMEBUFFER, 0);

  int width, height;
  glfwGetFramebufferSize(window, &width, &height);
  glViewport(0, 0, width, height);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  glBindVertexArray(vertexArrayID);

  glUseProgram(programID);

	glUniform1i(texture_UniformLocation, 0);
  texture.bind(GL_TEXTURE0);

	glDrawArrays(GL_TRIANGLES, 0, 6);
  glfwSwapBuffers(window);
}
