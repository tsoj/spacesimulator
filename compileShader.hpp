#pragma once

#include <iostream>
#include <string>
#include <vector>
#include <utility>
#include <initializer_list>
#include <GL/glew.h>

#include "io.hpp"

inline GLuint compileShader(std::initializer_list<std::pair<std::string, GLenum>> shaderList)
{
	GLuint programID = glCreateProgram();

	std::vector<std::pair<std::string, GLuint>> shaderIDs = std::vector<std::pair<std::string, GLuint>>();

	for(auto shader : shaderList)
	{

		GLuint shaderID = glCreateShader(shader.second);
		shaderIDs.emplace_back(shader.first, shaderID);
		std::string shaderCode = IO::readFile(shader.first);
		const char* adapter = shaderCode.data();
		glShaderSource(shaderID, 1, &adapter, 0);
		glCompileShader(shaderID);
	  glAttachShader(programID, shaderID);
	}

  glLinkProgram(programID);

	std::string error = "";

	for(auto shader : shaderIDs)
	{
		GLint success = 0;
	  glGetShaderiv(shader.second, GL_COMPILE_STATUS, &success);
	  if(success == GL_FALSE)
	  {
	    GLint maxLength = 0;
		  glGetShaderiv(shader.second, GL_INFO_LOG_LENGTH, &maxLength);
	    std::vector<GLchar> errorLog(maxLength);

	    glGetShaderInfoLog(shader.second, maxLength, &maxLength, &errorLog[0]);

	    error += "Failed to compile " + shader.first + ":\n" + std::string(errorLog.data()) + "\n";
	  }

		glDetachShader(programID, shader.second);
		glDeleteShader(shader.second);
	}

	GLint isLinked = 0;
	glGetProgramiv(programID, GL_LINK_STATUS, (int*)&isLinked);
	if(isLinked == GL_FALSE)
	{
		GLint maxLength = 0;
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &maxLength);
		std::vector<GLchar> errorLog(maxLength);

		glGetProgramInfoLog(programID, maxLength, &maxLength, &errorLog[0]);

		error += "Failed to link shader program:\n" + std::string(errorLog.data()) + "\n";
	}

	if(error != "")
	{
		glDeleteProgram(programID);
		throw std::runtime_error("\n"+error);
	}

	return programID;
}
