#include "GameClass.hpp"
#include "Renderable.hpp"
#include "types.hpp"
#include "drawTexture.hpp"
#include "GLError.hpp"
#include "util.hpp"
#include "GL.hpp"

#include <glm/gtc/matrix_transform.hpp>
#include <limits>

namespace
{
  glm::mat4 getModelToWorld(glm::vec3 offset, ecs::Entity<const EntityManager> entity)
  {
    glm::mat4 modelRotation;
    if(entity.hasComponents<Orientation>())
    {
      modelRotation = entity.getComponent<Orientation>().matrix;
    }
    else
    {
      modelRotation = glm::mat4(1.0);
    }
    glm::mat4 modelTranslation;
    if(entity.hasComponents<Position>())
    {
      modelTranslation = glm::translate(glm::mat4(1.0), entity.getComponent<Position>().coords - offset);
    }
    else
    {
      modelTranslation = glm::mat4(1.0);
    }
    return modelTranslation * modelRotation;
  }

  glm::mat4 getWorldToProjection(
    glm::vec3 offset,
    glm::float32 fieldOfView,
    glm::float32 aspectRatio,
    glm::float32 nearPlane,
    glm::float32 farPlane,
    glm::vec3 cameraPosition,
    glm::vec3 viewDirection,
    glm::vec3 up
  )
  {
    return
      glm::perspective(fieldOfView, aspectRatio,nearPlane, farPlane) *
      glm::lookAt(cameraPosition - offset, cameraPosition - offset + viewDirection, up);
  }

  auto getLightPerspectiveData(
    const glm::vec3 cameraViewFrustumCoords[8],
    const glm::vec3& lightPosition
  )
  {
    struct Result
    {
      glm::float32 angle;
      glm::float32 farPlane;
      glm::float32 nearPlane;
      glm::vec3 viewDirection;
      glm::vec3 up;
    };
    glm::float32 angle;
    glm::float32 farPlane;
    glm::float32 nearPlane;
    glm::vec3 lightViewDirection;
    glm::vec3 lightUp;
    lightViewDirection = glm::normalize(
      cameraViewFrustumCoords[0]+
      cameraViewFrustumCoords[1]+
      cameraViewFrustumCoords[2]+
      cameraViewFrustumCoords[3]+
      cameraViewFrustumCoords[4]+
      cameraViewFrustumCoords[5]+
      cameraViewFrustumCoords[6]+
      cameraViewFrustumCoords[7]-
      lightPosition*8.0f
    );
    lightUp = glm::vec3(-lightViewDirection.y, lightViewDirection.x, lightViewDirection.z);
    glm::float32 biggestAngle = 0.0;
    farPlane = 0.0;
    nearPlane = std::numeric_limits<glm::float32>::infinity();
    // TODO: fix nearPlane
    for(size_t i = 0; i<8; i++)
    {
      glm::float32 currentAngle =
        glm::acos(
          glm::abs(glm::dot(lightViewDirection, cameraViewFrustumCoords[i]-lightPosition))/
          (glm::length(lightViewDirection)*glm::length(cameraViewFrustumCoords[i]-lightPosition))
        );
      if(currentAngle>biggestAngle)
      {
        biggestAngle = currentAngle;
      }
      glm::float32 distance = glm::distance(lightPosition, cameraViewFrustumCoords[i]);
      if(distance>farPlane)
      {
        farPlane = distance;
      }
      if(distance<nearPlane)
      {
        nearPlane = distance;
      }
    }
    angle = 2.0*biggestAngle;
    return Result{angle, farPlane, nearPlane, lightViewDirection, lightUp};
  }

  constexpr int NUM_CASCADES = 8;
  constexpr glm::float32 cascades[NUM_CASCADES+1] = {0.0, 10.0, 20.0, 40.0, 100.0, 200.0, 800.0, 5000.0, 10000.0};

  glm::vec3 global_lightPosition = glm::vec3(-30000.0, 10000.0, -10.0);

  GL::Framebuffer* global_gBuffer = nullptr;
  GL::Texture2D* global_gPosition = nullptr;
  GL::Texture2D* global_gDiffuse = nullptr;
  GL::Texture2D* global_gNormal = nullptr;

  GL::Renderbuffer* global_depthBuffer = nullptr;

  GL::Framebuffer* global_imageTargetBuffer = nullptr;
  GL::Texture2D* global_imageTarget = nullptr;

  constexpr GLuint SHADOW_WIDTH=1024, SHADOW_HEIGHT=1024;
  constexpr glm::float32 MAX_SHADOW_DISTANCE=1000.0;

  GL::Framebuffer* global_depthMapBuffer[NUM_CASCADES];
  GL::Texture2DArray* global_depthMap;

  GL::Framebuffer* global_gShadowBuffer = nullptr;
  GL::Texture2D* global_gShadow = nullptr;

  void geometryPass(
    const ecs::EntityManager& entityManager,
    int width,
    int height,
    const Camera& camera,
    GL::Framebuffer* gBuffer
  )
  {
    static GL::Shader program = GL::Shader({{"shader/gBuffer.vert", GL_VERTEX_SHADER},{"shader/gBuffer.frag", GL_FRAGMENT_SHADER}});
    STATIC_CALL(program.registerUniform("modelToWorld"));
    STATIC_CALL(program.registerUniform("worldToProjection"));
    STATIC_CALL(program.registerUniform("diffuseTexture"));
    STATIC_CALL(program.registerUniform("normalMap"));

    glViewport(0, 0, width, height);

    gBuffer->bind();
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glm::mat4 worldToProjection = getWorldToProjection(
      camera.position,
      glm::radians(camera.fieldOfView),
      glm::float32(width)/glm::float32(height),
      camera.nearPlane,
      camera.farPlane,
      camera.position,
      camera.viewDirection,
      camera.up
    );

    int i = 0;

    program.use();

    for(auto entity : entityManager.iterator<Renderable, Position, Orientation>())
    {
      glm::mat4 modelToWorld = getModelToWorld(camera.position, entity);
      for(auto mesh : entity.getComponent<Renderable>().staticMeshs)
      {

        program.uniform("modelToWorld", modelToWorld);
        program.uniform("worldToProjection", worldToProjection);

        int textureCounter = 0;

        program.uniform("diffuseTexture", textureCounter);
        glActiveTexture(GL_TEXTURE0 + textureCounter);
        glBindTexture(GL_TEXTURE_2D, mesh.getDiffuseTextureID());
        textureCounter += 1;

        program.uniform("normalMap", textureCounter);
        glActiveTexture(GL_TEXTURE0 + textureCounter);
        glBindTexture(GL_TEXTURE_2D, mesh.getNormalMapID());
        textureCounter += 1;

        glBindVertexArray(mesh.getVertexArrayID());
        glDrawArrays(GL_TRIANGLES, 0, mesh.getNumVertices());
      }
      i++;
    }
  }

  glm::mat4 renderDepthMap(
    GL::Framebuffer* depthMapBuffer,
    GLuint width,
    GLuint height,
    glm::float32 angle,
    glm::float32 nearPlane,
    glm::float32 farPlane,
    glm::vec3 viewDirection,
    glm::vec3 up,
    glm::vec3 cameraPosition,
    glm::vec3 offset,
    const ecs::EntityManager& entityManager
  )
  {
    static GL::Shader program = GL::Shader({
      {"shader/depthMap.vert", GL_VERTEX_SHADER},
      {"shader/depthMap.frag", GL_FRAGMENT_SHADER}
    });
    STATIC_CALL(program.registerUniform("modelToWorld"));
    STATIC_CALL(program.registerUniform("worldToProjection"));

    glCullFace(GL_BACK);

    depthMapBuffer->bind();
    glViewport(0, 0, width, height);

    glClear(GL_DEPTH_BUFFER_BIT);

    const glm::mat4 worldToView = getWorldToProjection(
      offset,
      angle,
      glm::float32(width)/glm::float32(height),
      nearPlane,
      farPlane,
      cameraPosition,
      viewDirection,
      up
    );

    for(auto entity : entityManager.iterator<Renderable>())
    {
      glm::mat4 modelToWorld = getModelToWorld(offset, entity);
      for(auto mesh : entity.getComponent<Renderable>().staticMeshs)
      {
        glBindVertexArray(mesh.getVertexArrayID());

        program.use();

        program.uniform("modelToWorld", modelToWorld);
        program.uniform("worldToProjection", worldToView);

        glDrawArrays(GL_TRIANGLES, 0, mesh.getNumVertices());
      }
    }

    glBindFramebuffer(GL_FRAMEBUFFER, 0);

    glCullFace(GL_FRONT);

    return worldToView;
  }

  void renderShadow(
    GL::Framebuffer* shadowBuffer,
    const Camera& camera,
    GLuint width,
    GLuint height,
    GL::Texture2DArray* depthMap,
    glm::mat4 worldToLight[NUM_CASCADES],
    const ecs::EntityManager& entityManager
  )
  {
    static GL::Shader program = GL::Shader({{"shader/shadow.vert", GL_VERTEX_SHADER},{"shader/shadow.frag", GL_FRAGMENT_SHADER}});
    STATIC_CALL(program.registerUniform("modelToWorld"));
    STATIC_CALL(program.registerUniform("worldToProjection"));
    STATIC_CALL(program.registerUniform("depthMap"));
    STATIC_CALL(program.registerUniform("modelToWorld"));
    STATIC_CALL(program.registerUniform("worldToLight[0]"));
    STATIC_CALL(program.registerUniform("cascades[0]"));

    shadowBuffer->bind();
    glViewport(0, 0, width, height);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    const glm::mat4 worldToProjection = getWorldToProjection(
      camera.position,
      glm::radians(camera.fieldOfView),
      glm::float32(width)/glm::float32(height),
      camera.nearPlane,
      camera.farPlane,
      camera.position,
      camera.viewDirection,
      camera.up
    );

    for(auto entity : entityManager.iterator<Renderable, Position, Orientation>())
    {
      glm::mat4 modelToWorld = getModelToWorld(camera.position, entity);
      for(auto mesh : entity.getComponent<Renderable>().staticMeshs)
      {
        glBindVertexArray(mesh.getVertexArrayID());

        program.use();

        program.uniform("modelToWorld", modelToWorld);
        program.uniform("worldToProjection", worldToProjection);

        program.uniform("worldToLight[0]", NUM_CASCADES, worldToLight);
        program.uniform("cascades[0]", NUM_CASCADES, &cascades[1]);

        int textureCounter = 0;
        program.uniform("depthMap", textureCounter);
        depthMap->bind(GL_TEXTURE0 + textureCounter);
        textureCounter += 1;

        glDrawArrays(GL_TRIANGLES, 0, mesh.getNumVertices());
      }
    }
  }

  void shadowPass(
    const ecs::EntityManager& entityManager,
    GLFWwindow* window,
    const Camera& camera,
    GL::Texture2DArray* depthMap,
    GL::Framebuffer* depthMapBuffer[NUM_CASCADES],
    GL::Framebuffer* gShadowBuffer
  )
  {
    int width, height;
    glfwGetFramebufferSize(window, &width, &height);
    glm::float32 aspectRatio = GLfloat(width)/GLfloat(height);

    glm::mat4 worldToLight[NUM_CASCADES];

    glm::vec3 h_v = glm::normalize(camera.viewDirection);
    glm::vec3 w_v = glm::normalize(glm::cross(camera.up, camera.viewDirection));
    glm::vec3 u_v = glm::normalize(glm::cross(w_v, camera.viewDirection));

    for(int cascade = 0; cascade<NUM_CASCADES; cascade++)
    {
      glm::float32 h_a = cascades[cascade+1];
      glm::float32 u_a = glm::tan(camera.fieldOfView/2.0)*h_a;
      glm::float32 w_a = u_a*aspectRatio;

      glm::float32 h_a_m = cascades[cascade];
      glm::float32 u_a_m = glm::tan(camera.fieldOfView/2.0)*h_a_m;
      glm::float32 w_a_m = u_a_m*aspectRatio;

      glm::vec3 cameraViewFrustumCoords[8];
      cameraViewFrustumCoords[0] = camera.position + h_v*h_a_m + u_v*u_a_m + w_v*w_a_m;
      cameraViewFrustumCoords[1] = camera.position + h_v*h_a_m - u_v*u_a_m + w_v*w_a_m;
      cameraViewFrustumCoords[2] = camera.position + h_v*h_a_m + u_v*u_a_m - w_v*w_a_m;
      cameraViewFrustumCoords[3] = camera.position + h_v*h_a_m - u_v*u_a_m - w_v*w_a_m;
      cameraViewFrustumCoords[4] = camera.position + h_v*h_a + u_v*u_a + w_v*w_a;
      cameraViewFrustumCoords[5] = camera.position + h_v*h_a - u_v*u_a + w_v*w_a;
      cameraViewFrustumCoords[6] = camera.position + h_v*h_a + u_v*u_a - w_v*w_a;
      cameraViewFrustumCoords[7] = camera.position + h_v*h_a - u_v*u_a - w_v*w_a;

      auto lightData = getLightPerspectiveData(
        cameraViewFrustumCoords,
        global_lightPosition
      );

      if(not (lightData.angle > glm::radians(180.0f) || lightData.angle < glm::radians(0.0f)) )
      {
        worldToLight[cascade] = renderDepthMap(
          depthMapBuffer[cascade],
          SHADOW_WIDTH,
          SHADOW_HEIGHT,
          lightData.angle,
          lightData.farPlane-MAX_SHADOW_DISTANCE >= 0.1f ? lightData.farPlane-MAX_SHADOW_DISTANCE : 0.1f, // TODO: make that better
          lightData.farPlane,
          lightData.viewDirection,
          lightData.up,
          global_lightPosition,
          camera.position,
          entityManager
        );
      }
    }

    renderShadow(
      gShadowBuffer,
      camera,
      width,
      height,
      depthMap,
      worldToLight,
      entityManager
    );
  }

  void lightingPass(
    const Camera& camera,
    GLFWwindow* window,
    GL::Texture2D* gPosition,
    GL::Texture2D* gDiffuse,
    GL::Texture2D* gNormal,
    GL::Texture2D* gShadow,
    GL::Framebuffer* imageTargetBuffer
  )
  {
    static GL::Shader program = GL::Shader({{"shader/lighting.vert", GL_VERTEX_SHADER},{"shader/lighting.frag", GL_FRAGMENT_SHADER}});
    // TODO: do this only statically
    STATIC_CALL(program.registerUniform("position"));
    STATIC_CALL(program.registerUniform("diffuse"));
    STATIC_CALL(program.registerUniform("normal"));
    STATIC_CALL(program.registerUniform("shadow"));
    STATIC_CALL(program.registerUniform("lightPosition_WorldSpace"));

    const static glm::vec2 verts[] =
  	{
  	  glm::vec2(-1.0, 1.0),glm::vec2(0.0, 0.0),  glm::vec2(-1.0, -1.0),glm::vec2(0.0, -1.0),  glm::vec2(1.0, -1.0),glm::vec2(1.0, -1.0),
  	  glm::vec2(1.0, -1.0),glm::vec2(1.0, -1.0),  glm::vec2(1.0, 1.0),glm::vec2(1.0, 0.0),  glm::vec2(-1.0, 1.0),glm::vec2(0.0, 0.0)
      // vertexcoords     ,texturecoords
  	};
    static GLuint vertexBufferID;
    static GLuint vertexArrayID;
    static auto init = []()
    {
      glGenVertexArrays(1, &vertexArrayID);
      glBindVertexArray(vertexArrayID);

      glGenBuffers(1, &vertexBufferID);
      glBindBuffer(GL_ARRAY_BUFFER, vertexBufferID);

      glBufferData(GL_ARRAY_BUFFER, sizeof(verts) , verts, GL_STATIC_DRAW);
    	glEnableVertexAttribArray(0);
    	glVertexAttribPointer(
    		0,                  											// attribute 0. No particular reason for 0, but must match the layout in the shader.
    		2,     																		// size
    		GL_FLOAT,          												// type
    		GL_FALSE,																	// normalized
    		sizeof(glm::vec2)*2,											 // stride
    		(void*)0            											// array buffer offset
    	);
    	glEnableVertexAttribArray(1);
    	glVertexAttribPointer(
    		1,                  											// attribute 0. No particular reason for 0, but must match the layout in the shader.
    		2,     																		// size
    		GL_FLOAT,          												// type
    		GL_FALSE,																	// normalized
    		sizeof(glm::vec2)*2,											// stride
    		(void*)sizeof(glm::vec2)            			// array buffer offset
    	);
    };
    STATIC_CALL(init());

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    imageTargetBuffer->bind();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, width, height);

    glBindVertexArray(vertexArrayID);

    program.use();

    auto lightPosition = global_lightPosition - camera.position;
    program.uniform("lightPosition_WorldSpace", lightPosition);

    int textureCounter = 0;
    program.uniform("position", textureCounter);
    gPosition->bind(GL_TEXTURE0 + textureCounter);
    textureCounter += 1;

    program.uniform("diffuse", textureCounter);
    gDiffuse->bind(GL_TEXTURE0 + textureCounter);
    textureCounter += 1;

    program.uniform("normal", textureCounter);
    gNormal->bind(GL_TEXTURE0 + textureCounter);
    textureCounter += 1;

    program.uniform("shadow", textureCounter);
    gShadow->bind(GL_TEXTURE0 + textureCounter);
    textureCounter += 1;

  	glDrawArrays(GL_TRIANGLES, 0, 6);
  }

  void initRenderer(
    GLFWwindow* window,
    GL::Framebuffer*& gBuffer,
    GL::Texture2D*& gPosition,
    GL::Texture2D*& gDiffuse,
    GL::Texture2D*& gNormal,
    GL::Framebuffer*& gShadowBuffer,
    GL::Texture2D*& gShadow,
    GL::Framebuffer*& imageTargetBuffer,
    GL::Texture2D*& imageTarget,
    GL::Renderbuffer*& depthBuffer,
    GL::Texture2DArray*& depthMap,
    GL::Framebuffer* depthMapBuffer[NUM_CASCADES]
  )
  {
    GLError::clearError();

    int width, height;
    glfwGetFramebufferSize(window, &width, &height);

    depthBuffer = new GL::Renderbuffer(GL_DEPTH_COMPONENT32F, width, height);



    // position color buffer
    gPosition = new GL::Texture2D(GL_RGB16F, width, height, GL_RGB, GL_FLOAT);
    gPosition->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gPosition->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gPosition->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    gPosition->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // normal buffer
    gNormal = new GL::Texture2D(GL_RGB16F, width, height, GL_RGB, GL_FLOAT);
    gNormal->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gNormal->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gNormal->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    gNormal->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // color VertexBuffer
    gDiffuse = new GL::Texture2D(GL_RGBA, width, height, GL_RGBA, GL_UNSIGNED_BYTE);
    gDiffuse->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gDiffuse->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gDiffuse->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    gDiffuse->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // create gBuffer
    gBuffer = new GL::Framebuffer();
    gBuffer->attach(*gPosition, GL_COLOR_ATTACHMENT0);
    gBuffer->attach(*gNormal, GL_COLOR_ATTACHMENT1);
    gBuffer->attach(*gDiffuse, GL_COLOR_ATTACHMENT2);
    gBuffer->attach(*depthBuffer, GL_DEPTH_ATTACHMENT);
    gBuffer->setDrawBuffer({GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2});
    // finally check if framebuffer is complete
    GLError::checkFramebufferStatus("gBuffer");
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    // image target
    imageTarget = new GL::Texture2D(GL_RGBA, width, height, GL_RGBA, GL_UNSIGNED_BYTE);
    imageTarget->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    imageTarget->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    imageTarget->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    imageTarget->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    imageTargetBuffer = new GL::Framebuffer();
    imageTargetBuffer->attach(*imageTarget, GL_COLOR_ATTACHMENT0);
    imageTargetBuffer->attach(*depthBuffer, GL_DEPTH_ATTACHMENT);
    imageTargetBuffer->setDrawBuffer({GL_COLOR_ATTACHMENT0});
    // finally check if framebuffer is complete
    GLError::checkFramebufferStatus("imageTargetBuffer");
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    // shadow buffer
    gShadow = new GL::Texture2D(GL_RGB, width, height, GL_RGB, GL_UNSIGNED_BYTE);
    gShadow->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    gShadow->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    gShadow->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    gShadow->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);

    // create gShadowBuffer
    gShadowBuffer = new GL::Framebuffer();
    gShadowBuffer->attach(*gShadow, GL_COLOR_ATTACHMENT0);
    gShadowBuffer->attach(*depthBuffer, GL_DEPTH_ATTACHMENT);
    gShadowBuffer->setDrawBuffer({GL_COLOR_ATTACHMENT0});
    // finally check if framebuffer is complete
    GLError::checkFramebufferStatus("gShadowBuffer");
    glBindFramebuffer(GL_FRAMEBUFFER, 0);


    // create depthMap
    depthMap = new GL::Texture2DArray(GL_DEPTH_COMPONENT32F, SHADOW_WIDTH, SHADOW_HEIGHT, NUM_CASCADES);
    depthMap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    depthMap->setParameter(GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    depthMap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    depthMap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    depthMap->setParameter(GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);
    depthMap->setParameter(GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL);

    for(int i = 0; i<NUM_CASCADES; i++)
    {
      // create depthMap buffer
      depthMapBuffer[i] = new GL::Framebuffer();
      depthMapBuffer[i]->attach(*depthMap, GL_DEPTH_ATTACHMENT, i);
      // finally check if framebuffer is complete
      GLError::checkFramebufferStatus("depthMapBuffer");
      glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }
  }
}

void GameClass::renderer(const GameClass &gameClass, UNUSED const std::chrono::nanoseconds& deltaTime)
{
  auto start = std::chrono::steady_clock::now();

  glfwMakeContextCurrent(gameClass.window); // TODO: check runtime cost

  STATIC_CALL(initRenderer(
    gameClass.window,
    global_gBuffer,
    global_gPosition,
    global_gDiffuse,
    global_gNormal,
    global_gShadowBuffer,
    global_gShadow,
    global_imageTargetBuffer,
    global_imageTarget,
    global_depthBuffer,
    global_depthMap,
    global_depthMapBuffer
  ));

  int width, height;
  glfwGetFramebufferSize(gameClass.window, &width, &height);

  shadowPass(
    gameClass.entityManager,
    gameClass.window,
    gameClass.camera,
    global_depthMap,
    global_depthMapBuffer,
    global_gShadowBuffer
  );

  geometryPass(
    gameClass.entityManager,
    width,
    height,
    gameClass.camera,
    global_gBuffer
  );

  lightingPass(
    gameClass.camera,
    gameClass.window,
    global_gPosition,
    global_gDiffuse,
    global_gNormal,
    global_gShadow,
    global_imageTargetBuffer
  );

  GLError::checkError();

  auto end = std::chrono::steady_clock::now();
  std::cout << "\033[0K\rframe time: " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << " ms" << std::flush;

  drawTexture(gameClass.window, *global_imageTarget);
}
