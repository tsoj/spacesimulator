#pragma once

#include <glm/glm.hpp>

struct Camera
{
  glm::vec3 position;
  glm::vec3 viewDirection;
  glm::vec3 up;
  glm::float32 fieldOfView; // degrees
  glm::float32 nearPlane;
  glm::float32 farPlane;
};
