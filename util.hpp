#pragma once

#define STATIC_CALL(func) \
{__attribute__((unused)) const static int dummy = (func, 0);}
