#include "GL.hpp"
#include "lodepng.hpp"
#include <iostream>

namespace GL
{
  Texture2D::Texture2D(GLint internalformat, GLsizei width, GLsizei height, GLenum format, GLenum type)
  {
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);
    glTexImage2D(GL_TEXTURE_2D, 0, internalformat, width, height, 0, format, type, NULL);
  }
  Texture2D::Texture2D(std::string path)
  {
    std::vector<unsigned char> image;
    unsigned int width, height;
    auto error = lodepng::decode(image, width, height, path);
    if(error)
    {
      throw std::runtime_error("Can't load image ["+std::string(path)+"]: decoder error " + std::to_string(error) + ": " + lodepng_error_text(error));
    }


    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D, ID);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data());
    //enable mipmapping
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_LOD_BIAS, -0.3f);
  }
  Texture2D::~Texture2D()
  {
    glDeleteTextures(1, &ID);
  }
  void Texture2D::setParameter(GLenum pname, GLint param)
  {
    glBindTexture(GL_TEXTURE_2D, ID);
    glTexParameteri(GL_TEXTURE_2D, pname, param);
  }
  void Texture2D::bind(GLenum textureUnit) const
  {
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D, ID);
  }

  Texture2DArray::Texture2DArray(GLenum internalformat, GLsizei width, GLsizei height, GLsizei depth)
  {
    glGenTextures(1, &ID);
    glBindTexture(GL_TEXTURE_2D_ARRAY, ID);
    glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, internalformat, width, height, depth);
  }
  Texture2DArray::~Texture2DArray()
  {
    glDeleteTextures(1, &ID);
  }
  void Texture2DArray::setParameter(GLenum pname, GLint param)
  {
    glBindTexture(GL_TEXTURE_2D_ARRAY, ID);
    glTexParameteri(GL_TEXTURE_2D_ARRAY, pname, param);
  }
  void Texture2DArray::bind(GLenum textureUnit) const
  {
    glActiveTexture(textureUnit);
    glBindTexture(GL_TEXTURE_2D_ARRAY, ID);
  }
}
