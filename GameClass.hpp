#pragma once

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <vector>
#include <thread>

#include "s.hpp"
#include "ec.hpp"
#include "Obj.hpp"
#include "compileShader.hpp"
#include "Camera.hpp"

using namespace ecs;

// TODO: add physics quantities
class GameClass
{
public:

  GameClass();

  ~GameClass();

  void mainLoop()
  {
    systemManager.enterState<RUNNING>();
    while(not terminated)
    {
      systemManager.runSystems();
    }
  }

  void terminate()
  {
    terminated = true;
  }

private:

  bool terminated = false;

  enum State { RUNNING, EXITING };

  EntityManager entityManager = EntityManager();
  SystemManager<GameClass> systemManager = SystemManager(*this);

  GLFWwindow* window;

  Camera camera = {
    glm::vec3(0.0, 0.0, 10.0), // position
    glm::vec3(0.0, 0.0, -1.0), // view direction
    glm::vec3(0.0, 1.0, 0.0), // up
    70.0, // field of view in degrees
    0.01, // near plane
    1000.0 // far plane
  };

  inline const static std::vector<const char*> requiredExtensions =
  {
  };

  static void inputHandler(const GameClass& gameClass, const std::chrono::nanoseconds& dt);
  static void initRunning(GameClass& gameClass);
  static void initExit(GameClass& gameClass);

  static void renderer(const GameClass &gameClass, const std::chrono::nanoseconds& deltaTime);
  static void phyics(GameClass &gameClass, const std::chrono::nanoseconds& deltaTime);
};
