- [ ] Deffered Rendering, Shadows, PBR, graphics
  - [x] entity-component library
  - [x] systemmanager with builtin parallel support
  - [x] .obj file loading
  - [x] basic defferend rendering with diffuse shading
  - [x] normalmapping, textures
  - [x] shadowmapping for one light
  - [x] code cleanup
  - [x] cascade shadows
  - [ ] opengl abstractions
  - [ ] multiple lights
  - [ ] solve z buffer precision for large distances
  - [ ] shadows of very far large objects
  - [ ] PBR
- [ ] Newtonian Physics
  - [ ] upgrade glm to support Heterogeneous Operations
  - [ ] Veclocity, Position
  - [ ] Forces
  - [ ] Gravitation
  - [ ] Rotation
- [ ] Collision
  - [ ] Collision Detection
  - [ ] Collision Response
  - [ ] Friction
- [ ] Procedural Planets
  - [ ] Model Generation
  - [ ] Elevation Data Generation
- [ ] Atmosphere
  - [ ] Physics
  - [ ] Shading
- [ ] transparency
- [ ] Particle Effects
- [ ] Model Animations
- [ ] no resource leaking (opengl)


#!!!!!!!!!
entity system, entities can get attached together.
systems, events overhaul
frameculling!!!!!